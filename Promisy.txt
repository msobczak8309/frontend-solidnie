function fakeRequest(fakeData, callback ){
   setTimeout(function(){
       callback( fakeData );
   },1000)
}

fakeRequest('blog post 1', function( data ){
   fakeRequest(data + ' - autor alice', function(data){
       fakeRequest(data + ', and friends', function(data){
           console.log(data)
       })
   })
})

// ================

function fakeRequest(fakeData, fakeError){
    return new Promise(function(resolve , reject){
        setTimeout(function(){
            if(fakeError){
           	   reject(fakeError)
            }else{
               resolve(fakeData)
            }
        },1000);
    }); 
}

fakeRequest({post:'Post 1'})
.then(function(post){ post.autor = ' autor posta 1 '; return post; })
.then(function(post){ return Promise.all([
        fakeRequest(Object.assign({}, post,{znajomi:' i znajomi'}), 'Bład, nie ma znajomych!' ),
        fakeRequest(Object.assign({}, post,{lajki: ' i lajki'}))
]) })
.then(function(data){ return  Object.assign({}, data[0], data[1]) })
.catch( function(err){ return 'Komunikat : nie udało sie' })
.then(function(data){ console.log(data);  });

//// 
function promisify(f){
  return function promisified(){
    var args = Array.prototype.slice.call(arguments);
    return new Promise(function(resolve,reject){
       f.apply(window, args.concat(resolve))
     })
  }
}
myFunc(param,param2, callback){ .. }

pFunc = promisify(myFunc)

pFunc(param,param2)
.then(callback)

